import time
import os
from time import gmtime, strftime

SENSORS = [
    ('framledning', '10.BE162A020800'),
    ('framledning_gv', '10.B50F2A020800')
    ('retur', '10.9F38BF020800')
]


def readValue( address ):
    try:
        file_to_read = os.path.join("/","mnt","1wire",address,"temperature")
        file_object = open( file_to_read, 'r')
        value = file_object.read()
        file_object.close()
        return value
    except IOError:
        return ''

while 1:
    time_stamp = strftime("%Y-%m-%d %H:%M", gmtime())
    
    values = []
    
    for sensor in SENSORS:
        values.append( readValue( sensor[1] ) )

    print(time_stamp + '|' + values[0] + '|' + values[1] + '|' + values[2])
    time.sleep(60) 





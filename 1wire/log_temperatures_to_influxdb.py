import time
import os
from time import gmtime, strftime
from influxdb import InfluxDBClient

# polling intervall in minutes
POLLING_INTERVALL = 5

# 1wire sensors to probe (name, address)
TEMP_SENSORS = [
    ('flow_line', '10.BE162A020800'),
    ('return_line', '10.9F38BF020800')
]

client = InfluxDBClient('localhost', 8086, 'logger', 'hur bra som helst', 'loggerdb')


def readTemperature(address):
    try:
        file_to_read = os.path.join("/", "mnt", "1wire", address, "temperature")
        file_object = open(file_to_read, 'r')
        value = float( file_object.read() )
        file_object.close()
        return value
    except IOError:
        return ''


def writeValuesToInflux(temp_values):
    json_body = [
        {
            "measurement": "temperature",
            "fields": {
                "flow_line": temp_values[0],
                "return": temp_values[1]
            }
        }
    ]

    client.write_points(json_body)


while 1:
    epoch = int(time.time())
    temp_values = []

    for sensor in TEMP_SENSORS:
        temp_values.append(readTemperature(sensor[1]))

    writeValuesToInflux(temp_values)

#    print(str(epoch) + '|' + values[0] + '|' + values[1]

    time.sleep(60 * POLLING_INTERVALL)
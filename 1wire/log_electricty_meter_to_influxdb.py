import time
import os
from time import gmtime, strftime
from influxdb import InfluxDBClient

# polling intervall in minutes
POLLING_INTERVALL = 1

# 1wire sensors to probe (name, address)
COUNTER_SENSOR = ('electricity_meter', '1D.1DDC0C000000')


client = InfluxDBClient('localhost', 8086, 'logger', 'hur bra som helst', 'loggerdb')


def readValue(address):
    try:
        file_to_read = os.path.join("/", "mnt", "1wire", address, "counter.A")
        file_object = open(file_to_read, 'r')
        value = int( file_object.read() )
        file_object.close()
        return value
    except IOError:
        return ''


def writeValuesToInflux(values):
    json_body = [
        {
            "measurement": "electricty",
            "fields": {
                "raw_counter": values[0],
                "current_consumption": values[1]
            }
        }
    ]

    client.write_points(json_body)


current_consumption = 0
counter = -1
while 1:
    epoch = int(time.time())

    counter_last_probe = counter
    counter = readValue( COUNTER_SENSOR[1] )

    # Compute current consumption
    if (counter_last_probe > -1):
        current_consumption = counter - counter_last_probe

    writeValuesToInflux([counter, current_consumption])

    #print(str(epoch) + '|' + str(counter) + '|' + str(current_consumption) )

    time.sleep(60 * POLLING_INTERVALL)
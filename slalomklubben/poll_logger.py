# coding=utf-8

import urllib2
import time
import sys
from influxdb import InfluxDBClient
from time import gmtime, strftime

# Parameters
POLL_INTERVALL_IN_MINUTES = 5
PUMP_UPPER_WARN_LEVEL = 95
PUMP_ALERT_WHEN_DELTA_GREATER_THAN = 0.5

# Static definitions
URL = "http://e-logger.se/e-logger.js?xvG6Fl8BYx"
VATTENPUMP = "<td>Vattenpump</td><td class='chv'>"
UTE_TEMP = "<td>Utetemp</td><td class='chv'>"

# Instansiate influx client
client = InfluxDBClient('localhost', 8086, 'logger', 'hur bra som helst', 'loggerdb')


def find_and_extract_value(content, name):
    start = content.find(name) + len(name)
    stop = content[start:].find("</td>")
    return content[start:start + stop]


def read_values():
    vattenpump = 0
    temp = 0
    try:
        content = urllib2.urlopen(URL).read()
        vattenpump = float(find_and_extract_value(content, VATTENPUMP))
        temp = float(find_and_extract_value(content, UTE_TEMP))

    except IOError:
        return None

    return vattenpump, temp


def compute_rolling_average(old_average, new_value):
    # New average = old average * (n-1)/n + new value /n
    n = 20
    return old_average * (n - 1) / n + new_value / n


def write_values_temp_to_influx(value):
    json_body = [
        {
            "measurement": "temperature",
            "fields": {
                "outdoor_temp": value
            }
        }
    ]

    client.write_points(json_body)

def print_message(message):
    time_stamp = strftime("%Y-%m-%d %H:%M", gmtime())
    print str(time_stamp) + " - " + message

def main():

    pump_running = 0

    prevVattenpump = 0.0
    averageVattenpump = 0.0
    print_message('starting...')
    while 1:

        vattenpump, temp = read_values()

        #print_message(attenpump, temp

        write_values_temp_to_influx(temp)

        if vattenpump > 1.0:
            # Pump is running
            if not pump_running:
                print_message('Pump start detected')
                pump_running = 1

                # no reason doing any more checks while starting...
                continue

            # check that we are not taking out to much effect
            if vattenpump > PUMP_UPPER_WARN_LEVEL:
                print_message('WARNING pump running at ' + str(vattenpump) + "% it is above the recommended " + str(PUMP_UPPER_WARN_LEVEL) + "%")

            if prevVattenpump > 0 and abs(prevVattenpump-vattenpump) > PUMP_ALERT_WHEN_DELTA_GREATER_THAN:
                print_message('Pumpen strömförbrukning rör på sig - har någon kanon börjat frysa eller läcker det någonstans?')

            if temp > -2.5:
                print_message('Snöläggningen är igång men det är bara ' + str(temp) + ' grader - det bör vara kallare än -3')

            if temp > 0:
                print_message('Det är ,' + str(temp) + ' grader, vilket är för varmt för att försöka lägga snö!')

            prevVattenpump = vattenpump
            averageVattenpump = compute_rolling_average(averageVattenpump,vattenpump)


        else:
            # pump not running
            if pump_running:
                print_message('Pump stop detected')
                pump_running = 0
                prevVattenpump = 0
                averageVattenpump = 0

                # no reason doing any more checks while stopping...
                continue

            if temp < -3.5:
                print_message('Det är ' + str(temp) + ' grader och snökanonerna är inte igång - sätt fart!')


        time.sleep(POLL_INTERVALL_IN_MINUTES * 60)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print_message('terminating')
        sys.exit(0)